<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'hive_career',
   'tx_hivecareer_domain_model_joboffer'
);

$sModel = basename(__FILE__, '.php');


$GLOBALS['TCA'][$sModel]['columns']['employmenttype'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_joboffer.employmenttype',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['Full-time', 'FULL_TIME'],
            ['Part-time', 'PART_TIME'],
            ['Contractor', 'CONTRACTOR'],
            ['Temporary', 'TEMPORARY'],
            ['Intern', 'INTERN'],
            ['Volunteer', 'VOLUNTEER'],
            ['Per diem', 'PER_DIEM'],
            ['Other', 'OTHER'],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => 'required'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['external_link'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_joboffer.external_link',
    'config' => [
        'type' => 'input',
        'renderType' => 'inputLink',
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['contact'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_joboffer.contact',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectMultipleSideBySide',
        'foreign_table' => 'tx_hivecptcntcontactperson_domain_model_contactperson',
        'default' => 0,
        'size' => 10,
        'autoSizeMax' => 30,
        'maxitems' => 1,
        'multiple' => 0,
        'fieldControl' => [
            'editPopup' => [
                'disabled' => false,
            ],
            'addRecord' => [
                'disabled' => false,
            ],
            'listModule' => [
                'disabled' => true,
            ],
        ],
    ],
];

/*
ToDO: slug / pathSegment Handler (use from news EXT)
*/
$GLOBALS['TCA'][$sModel]['columns']['path_segment']['config'] = [
    'type' => 'slug',
    'size' => 50,
    'generatorOptions' => [
        'fields' => ['title'],
        'replacements' => [
            '/' => '-'
        ],
    ],
    'fallbackCharacter' => '-',
    'eval' => 'unique',
    'default' => ''
];

$GLOBALS['TCA'][$sModel]['columns']['googleforjobs'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_joboffer.googleforjobs',
    'config' => [
        'type' => 'inline',
        'foreign_table' => 'tx_hivecareer_domain_model_googleforjobs',
        'foreign_field' => 'joboffer',
        'maxitems' => 1,
        'appearance' => [
            'collapseAll' => 0,
            'levelLinksPosition' => 'top',
            'showSynchronizationLink' => 1,
            'showPossibleLocalizationRecords' => 1,
            'showAllLocalizationLink' => 1
        ],
    ],
];



$sTypes1ShowitemTemp = $GLOBALS['TCA'][$sModel]['types']['1']['showitem'];
$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = str_replace("googleforjobs,", "--div--;LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_joboffer.googleforjobs, googleforjobs,", $sTypes1ShowitemTemp);
