<?php
namespace HIVE\HiveCareer\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class JobOfferTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \HIVE\HiveCareer\Domain\Model\JobOffer
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCareer\Domain\Model\JobOffer();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );
    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStartDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStartDate()
        );
    }

    /**
     * @test
     */
    public function setStartDateForDateTimeSetsStartDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStartDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'startDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmploymenttypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmploymenttype()
        );
    }

    /**
     * @test
     */
    public function setEmploymenttypeForStringSetsEmploymenttype()
    {
        $this->subject->setEmploymenttype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'employmenttype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCompanyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCompany()
        );
    }

    /**
     * @test
     */
    public function setCompanyForStringSetsCompany()
    {
        $this->subject->setCompany('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'company',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLocationReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLocation()
        );
    }

    /**
     * @test
     */
    public function setLocationForStringSetsLocation()
    {
        $this->subject->setLocation('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'location',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIntroReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIntro()
        );
    }

    /**
     * @test
     */
    public function setIntroForStringSetsIntro()
    {
        $this->subject->setIntro('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'intro',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTaskReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTask()
        );
    }

    /**
     * @test
     */
    public function setTaskForStringSetsTask()
    {
        $this->subject->setTask('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'task',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getProfileReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getProfile()
        );
    }

    /**
     * @test
     */
    public function setProfileForStringSetsProfile()
    {
        $this->subject->setProfile('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'profile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInformationReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInformation()
        );
    }

    /**
     * @test
     */
    public function setInformationForStringSetsInformation()
    {
        $this->subject->setInformation('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'information',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDownloadReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getDownload()
        );
    }

    /**
     * @test
     */
    public function setDownloadForFileReferenceSetsDownload()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setDownload($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'download',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getExternalLinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getExternalLink()
        );
    }

    /**
     * @test
     */
    public function setExternalLinkForStringSetsExternalLink()
    {
        $this->subject->setExternalLink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'externalLink',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getGoogleforjobsReturnsInitialValueForGoogleForJobs()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getGoogleforjobs()
        );
    }

    /**
     * @test
     */
    public function setGoogleforjobsForObjectStorageContainingGoogleForJobsSetsGoogleforjobs()
    {
        $googleforjob = new \HIVE\HiveCareer\Domain\Model\GoogleForJobs();
        $objectStorageHoldingExactlyOneGoogleforjobs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneGoogleforjobs->attach($googleforjob);
        $this->subject->setGoogleforjobs($objectStorageHoldingExactlyOneGoogleforjobs);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneGoogleforjobs,
            'googleforjobs',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addGoogleforjobToObjectStorageHoldingGoogleforjobs()
    {
        $googleforjob = new \HIVE\HiveCareer\Domain\Model\GoogleForJobs();
        $googleforjobsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $googleforjobsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($googleforjob));
        $this->inject($this->subject, 'googleforjobs', $googleforjobsObjectStorageMock);

        $this->subject->addGoogleforjob($googleforjob);
    }

    /**
     * @test
     */
    public function removeGoogleforjobFromObjectStorageHoldingGoogleforjobs()
    {
        $googleforjob = new \HIVE\HiveCareer\Domain\Model\GoogleForJobs();
        $googleforjobsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $googleforjobsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($googleforjob));
        $this->inject($this->subject, 'googleforjobs', $googleforjobsObjectStorageMock);

        $this->subject->removeGoogleforjob($googleforjob);
    }

    /**
     * @test
     */
    public function getContactReturnsInitialValueForContactperson()
    {
    }

    /**
     * @test
     */
    public function setContactForContactpersonSetsContact()
    {
    }
}
