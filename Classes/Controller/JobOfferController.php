<?php
namespace HIVE\HiveCareer\Controller;


/***
 *
 * This file is part of the "hive_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/
/**
 * JobOfferController
 */
class JobOfferController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * jobOfferRepository
     *
     * @var \HIVE\HiveCareer\Domain\Repository\JobOfferRepository
     */
    protected $jobOfferRepository = null;

    /**
     * @param \HIVE\HiveCareer\Domain\Repository\JobOfferRepository $jobOfferRepository
     */
    public function injectJobOfferRepository(\HIVE\HiveCareer\Domain\Repository\JobOfferRepository $jobOfferRepository)
    {
        $this->jobOfferRepository = $jobOfferRepository;
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $jobOffers = $this->jobOfferRepository->findAll();
        $this->view->assign('jobOffers', $jobOffers);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveCareer\Domain\Model\JobOffer $jobOffer
     * @return void
     */
    public function showAction(\HIVE\HiveCareer\Domain\Model\JobOffer $jobOffer)
    {

        /*set structureData for GoogleForJobs */
        $g4jobs_structuredData = $this->buildG4JobsStructuredData($jobOffer);
        $this->view->assign('jobOffer', $jobOffer);
        $this->view->assign('g4jobs_structuredData', $g4jobs_structuredData);
    }

    /**
     * @param \HIVE\HiveCareer\Domain\Model\JobOffer $jobOffer
     */
    protected function buildG4JobsStructuredData(\HIVE\HiveCareer\Domain\Model\JobOffer $jobOffer)
    {
        $structuredData = null;
        $jobOffer->getGoogleforjobs()->rewind();
        $oGoogleForJobs = $jobOffer->getGoogleforjobs()->current();

        if ($oGoogleForJobs) {
            $aGoogleForJobs = [];
            $aGoogleForJobs['@context'] = 'https://schema.org/';
            $aGoogleForJobs['@type'] = 'JobPosting';

            //Title
            $aGoogleForJobs['title'] = $oGoogleForJobs->getTitle();
            if ($oGoogleForJobs->getTitle() == '') {
                $aGoogleForJobs['title'] = $jobOffer->getTitle();
            }

            //Description
            $aGoogleForJobs['description'] = $oGoogleForJobs->getDescription();
            if ($oGoogleForJobs->getDescription() == '') {
                $desc = $jobOffer->getIntro() . $jobOffer->getTask() . $jobOffer->getProfile() . $jobOffer->getInformation();
                $aGoogleForJobs['description'] = $desc;
            }

            //Image
            if ($oGoogleForJobs->getHiringorganizationLogo() == '') {
                $aGoogleForJobs['image'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
            } else {
                $aGoogleForJobs['image'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $oGoogleForJobs->getHiringorganizationLogo()->getOriginalResource()->getPublicUrl();
            }

            //Employment Type
            $aGoogleForJobs['employmentType'] = $oGoogleForJobs->getEmploymenttype();
            if ($oGoogleForJobs->getEmploymenttype() == '') {
                $aGoogleForJobs['employmentType'] = $jobOffer->getEmploymenttype();
            }

            //Identifier
            $aGoogleForJobs['identifier']['@type'] = 'PropertyValue';
            $aGoogleForJobs['identifier']['name'] = $oGoogleForJobs->getIdentifierName();
            if ($oGoogleForJobs->getIdentifierName() == '') {
                $aGoogleForJobs['identifier']['name'] = $this->settings['googleforjobs']['identifier']['name'];
            }
            $aGoogleForJobs['identifier']['value'] = $oGoogleForJobs->getIdentifierValue();
            if ($oGoogleForJobs->getIdentifierValue() == '') {
                $aGoogleForJobs['identifier']['value'] = $this->settings['googleforjobs']['identifier']['value'];
            }

            //hiringOrganization
            $aGoogleForJobs['hiringOrganization']['@type'] = 'Organization';
            $aGoogleForJobs['hiringOrganization']['name'] = $oGoogleForJobs->getHiringorganizationName();
            if ($oGoogleForJobs->getHiringorganizationName() == '') {
                $aGoogleForJobs['hiringOrganization']['name'] = $this->settings['googleforjobs']['hiringOrganization']['name'];
            }
            $aGoogleForJobs['hiringOrganization']['sameAs'] = $oGoogleForJobs->getHiringorganizationWebsite();
            if ($oGoogleForJobs->getHiringorganizationWebsite() == '') {
                $aGoogleForJobs['hiringOrganization']['sameAs'] = $this->settings['googleforjobs']['hiringOrganization']['website'];
            }
            if ($oGoogleForJobs->getHiringorganizationLogo() == '') {
                $aGoogleForJobs['hiringOrganization']['logo'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
            } else {
                $aGoogleForJobs['hiringOrganization']['logo'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $oGoogleForJobs->getHiringorganizationLogo()->getOriginalResource()->getPublicUrl();
            }

            //jobLocation
            $aGoogleForJobs['jobLocation']['@type'] = 'Place';
            $aGoogleForJobs['jobLocation']['address']['@type'] = 'PostalAddress';
            $aGoogleForJobs['jobLocation']['address']['streetAddress'] = $oGoogleForJobs->getJoblocationAddressStreet();
            if ($oGoogleForJobs->getJoblocationAddressStreet() == '') {
                $aGoogleForJobs['jobLocation']['address']['streetAddress'] = $this->settings['googleforjobs']['jobLocation']['address']['street'];
            }
            $aGoogleForJobs['jobLocation']['address']['addressLocality'] = $oGoogleForJobs->getJoblocationAddressCity();
            if ($oGoogleForJobs->getJoblocationAddressCity() == '') {
                $aGoogleForJobs['jobLocation']['address']['addressLocality'] = $this->settings['googleforjobs']['jobLocation']['address']['city'];
            }
            $aGoogleForJobs['jobLocation']['address']['addressRegion'] = $oGoogleForJobs->getJoblocationAddressRegion();
            if ($oGoogleForJobs->getJoblocationAddressRegion() == '') {
                $aGoogleForJobs['jobLocation']['address']['addressRegion'] = $this->settings['googleforjobs']['jobLocation']['address']['region'];
            }
            $aGoogleForJobs['jobLocation']['address']['postalCode'] = $oGoogleForJobs->getJoblocationAddressPostalcode();
            if ($oGoogleForJobs->getJoblocationAddressPostalcode() == '') {
                $aGoogleForJobs['jobLocation']['address']['postalCode'] = $this->settings['googleforjobs']['jobLocation']['address']['postalCode'];
            }
            $aGoogleForJobs['jobLocation']['address']['addressCountry'] = $oGoogleForJobs->getJoblocationAddressCountry();
            if ($oGoogleForJobs->getJoblocationAddressCountry() == '') {
                $aGoogleForJobs['jobLocation']['address']['addressCountry'] = $this->settings['googleforjobs']['jobLocation']['address']['country'];
            }

            //baseSalary
            if ($oGoogleForJobs->getBasesalaryValue() > 0) {
                $aGoogleForJobs['baseSalary']['@type'] = 'MonetaryAmount';
                $aGoogleForJobs['baseSalary']['currency'] = $oGoogleForJobs->getBasesalaryCurrency();
                $aGoogleForJobs['baseSalary']['value']['@type'] = 'QuantitativeValue';
                $aGoogleForJobs['baseSalary']['value']['value'] = $oGoogleForJobs->getBasesalaryValue();
                $aGoogleForJobs['baseSalary']['value']['unitText'] = $oGoogleForJobs->getBasesalaryUnit();
            }

            //remoteJob
            if ($oGoogleForJobs->isRemotejob()) {
                $aGoogleForJobs['jobLocationType'] = 'TELECOMMUTE';
                $aGoogleForJobs['applicantLocationRequirements']['@type'] = 'Country';
                $aGoogleForJobs['applicantLocationRequirements']['name'] = $oGoogleForJobs->getRemotejobLocationRequirements();
            }

            //datePosted
            if ($oGoogleForJobs->getDatePosted()) {
                $aGoogleForJobs['datePosted'] = $oGoogleForJobs->getDatePosted()->format('Y-m-d');
            } else {

                //use current date
                $aGoogleForJobs['datePosted'] = date('c');
            }

            //validThrough
            if ($oGoogleForJobs->getValidThrough()) {
                $aGoogleForJobs['validThrough'] = $oGoogleForJobs->getValidThrough()->format('Y-m-d');
            }

            // make json
            $jsonData = json_encode($aGoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            // add script tag
            $structuredData = '<script type="application/ld+json">' . $jsonData . '</script>';
        } else {
            $aGoogleForJobs = [];
            $aGoogleForJobs['@context'] = 'https://schema.org/';
            $aGoogleForJobs['@type'] = 'JobPosting';

            //Title
            $aGoogleForJobs['title'] = $jobOffer->getTitle();

            //Description
            $aGoogleForJobs['description'] = $jobOffer->getIntro() . $jobOffer->getTask() . $jobOffer->getProfile() . $jobOffer->getInformation();

            //Image
            $aGoogleForJobs['image'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];

            //Employment Type
            $aGoogleForJobs['employmentType'] = $jobOffer->getEmploymenttype();


            //Identifier
            $aGoogleForJobs['identifier']['@type'] = 'PropertyValue';
            $aGoogleForJobs['identifier']['name'] = $this->settings['googleforjobs']['identifier']['name'];
            $aGoogleForJobs['identifier']['value'] = $this->settings['googleforjobs']['identifier']['value'];

            //hiringOrganization
            $aGoogleForJobs['hiringOrganization']['@type'] = 'Organization';
            $aGoogleForJobs['hiringOrganization']['name'] = $this->settings['googleforjobs']['hiringOrganization']['name'];
            $aGoogleForJobs['hiringOrganization']['sameAs'] = $this->settings['googleforjobs']['hiringOrganization']['website'];
            $aGoogleForJobs['hiringOrganization']['logo'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];

            //jobLocation
            $aGoogleForJobs['jobLocation']['@type'] = 'Place';
            $aGoogleForJobs['jobLocation']['address']['@type'] = 'PostalAddress';
            $aGoogleForJobs['jobLocation']['address']['streetAddress'] = $this->settings['googleforjobs']['jobLocation']['address']['street'];
            $aGoogleForJobs['jobLocation']['address']['addressLocality'] = $this->settings['googleforjobs']['jobLocation']['address']['city'];
            $aGoogleForJobs['jobLocation']['address']['addressRegion'] = $this->settings['googleforjobs']['jobLocation']['address']['region'];
            $aGoogleForJobs['jobLocation']['address']['postalCode'] = $this->settings['googleforjobs']['jobLocation']['address']['postalCode'];
            $aGoogleForJobs['jobLocation']['address']['addressCountry'] = $this->settings['googleforjobs']['jobLocation']['address']['country'];

            //datePosted
            $aGoogleForJobs['datePosted'] = date('c');

            // make json
            $jsonData = json_encode($aGoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            // add script tag
            $structuredData = '<script type="application/ld+json">' . $jsonData . '</script>';
        }
        return $structuredData;
    }
}
