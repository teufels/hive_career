<?php
namespace HIVE\HiveCareer\Domain\Repository;

use HIVE\HiveCareer\Domain\Model\JobOffer;

/***
 *
 * This file is part of the "hive_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/
/**
 * The repository for JobOffers
 */
class JobOfferRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];
}
