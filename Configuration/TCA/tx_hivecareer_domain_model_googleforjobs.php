<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs',
        'label' => 'backend_title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backend_title,title,description,employmenttype,identifier_name,identifier_value,hiringorganization_name,hiringorganization_website,joblocation_address_street,joblocation_address_postalcode,joblocation_address_city,joblocation_address_region,joblocation_address_country,basesalary_currency,basesalary_unit,remotejob_location_requirements',
        'iconfile' => 'EXT:hive_career/Resources/Public/Icons/tx_hivecareer_domain_model_googleforjobs.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, description, employmenttype, identifier_name, identifier_value, hiringorganization_name, hiringorganization_website, hiringorganization_logo, joblocation_address_street, joblocation_address_postalcode, joblocation_address_city, joblocation_address_region, joblocation_address_country, basesalary_currency, basesalary_unit, basesalary_value, remotejob, remotejob_location_requirements, date_posted, valid_through',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, description, employmenttype, identifier_name, identifier_value, hiringorganization_name, hiringorganization_website, hiringorganization_logo, joblocation_address_street, joblocation_address_postalcode, joblocation_address_city, joblocation_address_region, joblocation_address_country, basesalary_currency, basesalary_unit, basesalary_value, remotejob, remotejob_location_requirements, date_posted, valid_through, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hivecareer_domain_model_googleforjobs',
                'foreign_table_where' => 'AND {#tx_hivecareer_domain_model_googleforjobs}.{#pid}=###CURRENT_PID### AND {#tx_hivecareer_domain_model_googleforjobs}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'backend_title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.backend_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'employmenttype' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.employmenttype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'identifier_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.identifier_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'identifier_value' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.identifier_value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'hiringorganization_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'hiringorganization_website' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_website',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'hiringorganization_logo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_logo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'hiringorganization_logo',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'hiringorganization_logo',
                        'tablenames' => 'tx_hivecareer_domain_model_googleforjobs',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
            
        ],
        'joblocation_address_street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'joblocation_address_postalcode' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_postalcode',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'joblocation_address_city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'joblocation_address_region' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_region',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'joblocation_address_country' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'basesalary_currency' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_currency',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'basesalary_unit' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_unit',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'basesalary_value' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            ]
        ],
        'remotejob' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.remotejob',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'remotejob_location_requirements' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.remotejob_location_requirements',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'date_posted' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.date_posted',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
            ],
        ],
        'valid_through' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.valid_through',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
            ],
        ],
    
        'joboffer' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
