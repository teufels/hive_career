<?php
namespace HIVE\HiveCareer\Domain\Model;


/***
 *
 * This file is part of the "hive_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/
/**
 * GoogleForJobs
 */
class GoogleForJobs extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * backendTitle
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $backendTitle = '';

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * employmenttype
     * 
     * @var string
     */
    protected $employmenttype = '';

    /**
     * identifierName
     * 
     * @var string
     */
    protected $identifierName = '';

    /**
     * identifierValue
     * 
     * @var string
     */
    protected $identifierValue = '';

    /**
     * hiringorganizationName
     * 
     * @var string
     */
    protected $hiringorganizationName = '';

    /**
     * hiringorganizationWebsite
     * 
     * @var string
     */
    protected $hiringorganizationWebsite = '';

    /**
     * hiringorganizationLogo
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $hiringorganizationLogo = null;

    /**
     * joblocationAddressStreet
     * 
     * @var string
     */
    protected $joblocationAddressStreet = '';

    /**
     * joblocationAddressPostalcode
     * 
     * @var string
     */
    protected $joblocationAddressPostalcode = '';

    /**
     * joblocationAddressCity
     * 
     * @var string
     */
    protected $joblocationAddressCity = '';

    /**
     * joblocationAddressRegion
     * 
     * @var string
     */
    protected $joblocationAddressRegion = '';

    /**
     * joblocationAddressCountry
     * 
     * @var string
     */
    protected $joblocationAddressCountry = '';

    /**
     * basesalaryCurrency
     * 
     * @var string
     */
    protected $basesalaryCurrency = '';

    /**
     * basesalaryUnit
     * 
     * @var string
     */
    protected $basesalaryUnit = '';

    /**
     * basesalaryValue
     * 
     * @var float
     */
    protected $basesalaryValue = 0.0;

    /**
     * remotejob
     * 
     * @var bool
     */
    protected $remotejob = false;

    /**
     * remotejobLocationRequirements
     * 
     * @var string
     */
    protected $remotejobLocationRequirements = '';

    /**
     * datePosted
     * 
     * @var \DateTime
     */
    protected $datePosted = null;

    /**
     * validThrough
     * 
     * @var \DateTime
     */
    protected $validThrough = null;

    /**
     * Returns the backendTitle
     * 
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     * 
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the employmenttype
     * 
     * @return string $employmenttype
     */
    public function getEmploymenttype()
    {
        return $this->employmenttype;
    }

    /**
     * Sets the employmenttype
     * 
     * @param string $employmenttype
     * @return void
     */
    public function setEmploymenttype($employmenttype)
    {
        $this->employmenttype = $employmenttype;
    }

    /**
     * Returns the identifierName
     * 
     * @return string $identifierName
     */
    public function getIdentifierName()
    {
        return $this->identifierName;
    }

    /**
     * Sets the identifierName
     * 
     * @param string $identifierName
     * @return void
     */
    public function setIdentifierName($identifierName)
    {
        $this->identifierName = $identifierName;
    }

    /**
     * Returns the identifierValue
     * 
     * @return string $identifierValue
     */
    public function getIdentifierValue()
    {
        return $this->identifierValue;
    }

    /**
     * Sets the identifierValue
     * 
     * @param string $identifierValue
     * @return void
     */
    public function setIdentifierValue($identifierValue)
    {
        $this->identifierValue = $identifierValue;
    }

    /**
     * Returns the hiringorganizationName
     * 
     * @return string $hiringorganizationName
     */
    public function getHiringorganizationName()
    {
        return $this->hiringorganizationName;
    }

    /**
     * Sets the hiringorganizationName
     * 
     * @param string $hiringorganizationName
     * @return void
     */
    public function setHiringorganizationName($hiringorganizationName)
    {
        $this->hiringorganizationName = $hiringorganizationName;
    }

    /**
     * Returns the hiringorganizationWebsite
     * 
     * @return string $hiringorganizationWebsite
     */
    public function getHiringorganizationWebsite()
    {
        return $this->hiringorganizationWebsite;
    }

    /**
     * Sets the hiringorganizationWebsite
     * 
     * @param string $hiringorganizationWebsite
     * @return void
     */
    public function setHiringorganizationWebsite($hiringorganizationWebsite)
    {
        $this->hiringorganizationWebsite = $hiringorganizationWebsite;
    }

    /**
     * Returns the hiringorganizationLogo
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $hiringorganizationLogo
     */
    public function getHiringorganizationLogo()
    {
        return $this->hiringorganizationLogo;
    }

    /**
     * Sets the hiringorganizationLogo
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $hiringorganizationLogo
     * @return void
     */
    public function setHiringorganizationLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $hiringorganizationLogo)
    {
        $this->hiringorganizationLogo = $hiringorganizationLogo;
    }

    /**
     * Returns the joblocationAddressStreet
     * 
     * @return string $joblocationAddressStreet
     */
    public function getJoblocationAddressStreet()
    {
        return $this->joblocationAddressStreet;
    }

    /**
     * Sets the joblocationAddressStreet
     * 
     * @param string $joblocationAddressStreet
     * @return void
     */
    public function setJoblocationAddressStreet($joblocationAddressStreet)
    {
        $this->joblocationAddressStreet = $joblocationAddressStreet;
    }

    /**
     * Returns the joblocationAddressPostalcode
     * 
     * @return string $joblocationAddressPostalcode
     */
    public function getJoblocationAddressPostalcode()
    {
        return $this->joblocationAddressPostalcode;
    }

    /**
     * Sets the joblocationAddressPostalcode
     * 
     * @param string $joblocationAddressPostalcode
     * @return void
     */
    public function setJoblocationAddressPostalcode($joblocationAddressPostalcode)
    {
        $this->joblocationAddressPostalcode = $joblocationAddressPostalcode;
    }

    /**
     * Returns the joblocationAddressCity
     * 
     * @return string $joblocationAddressCity
     */
    public function getJoblocationAddressCity()
    {
        return $this->joblocationAddressCity;
    }

    /**
     * Sets the joblocationAddressCity
     * 
     * @param string $joblocationAddressCity
     * @return void
     */
    public function setJoblocationAddressCity($joblocationAddressCity)
    {
        $this->joblocationAddressCity = $joblocationAddressCity;
    }

    /**
     * Returns the joblocationAddressRegion
     * 
     * @return string $joblocationAddressRegion
     */
    public function getJoblocationAddressRegion()
    {
        return $this->joblocationAddressRegion;
    }

    /**
     * Sets the joblocationAddressRegion
     * 
     * @param string $joblocationAddressRegion
     * @return void
     */
    public function setJoblocationAddressRegion($joblocationAddressRegion)
    {
        $this->joblocationAddressRegion = $joblocationAddressRegion;
    }

    /**
     * Returns the joblocationAddressCountry
     * 
     * @return string $joblocationAddressCountry
     */
    public function getJoblocationAddressCountry()
    {
        return $this->joblocationAddressCountry;
    }

    /**
     * Sets the joblocationAddressCountry
     * 
     * @param string $joblocationAddressCountry
     * @return void
     */
    public function setJoblocationAddressCountry($joblocationAddressCountry)
    {
        $this->joblocationAddressCountry = $joblocationAddressCountry;
    }

    /**
     * Returns the basesalaryCurrency
     * 
     * @return string $basesalaryCurrency
     */
    public function getBasesalaryCurrency()
    {
        return $this->basesalaryCurrency;
    }

    /**
     * Sets the basesalaryCurrency
     * 
     * @param string $basesalaryCurrency
     * @return void
     */
    public function setBasesalaryCurrency($basesalaryCurrency)
    {
        $this->basesalaryCurrency = $basesalaryCurrency;
    }

    /**
     * Returns the basesalaryUnit
     * 
     * @return string $basesalaryUnit
     */
    public function getBasesalaryUnit()
    {
        return $this->basesalaryUnit;
    }

    /**
     * Sets the basesalaryUnit
     * 
     * @param string $basesalaryUnit
     * @return void
     */
    public function setBasesalaryUnit($basesalaryUnit)
    {
        $this->basesalaryUnit = $basesalaryUnit;
    }

    /**
     * Returns the basesalaryValue
     * 
     * @return float $basesalaryValue
     */
    public function getBasesalaryValue()
    {
        return $this->basesalaryValue;
    }

    /**
     * Sets the basesalaryValue
     * 
     * @param float $basesalaryValue
     * @return void
     */
    public function setBasesalaryValue($basesalaryValue)
    {
        $this->basesalaryValue = $basesalaryValue;
    }

    /**
     * Returns the remotejob
     * 
     * @return bool $remotejob
     */
    public function getRemotejob()
    {
        return $this->remotejob;
    }

    /**
     * Sets the remotejob
     * 
     * @param bool $remotejob
     * @return void
     */
    public function setRemotejob($remotejob)
    {
        $this->remotejob = $remotejob;
    }

    /**
     * Returns the boolean state of remotejob
     * 
     * @return bool
     */
    public function isRemotejob()
    {
        return $this->remotejob;
    }

    /**
     * Returns the remotejobLocationRequirements
     * 
     * @return string $remotejobLocationRequirements
     */
    public function getRemotejobLocationRequirements()
    {
        return $this->remotejobLocationRequirements;
    }

    /**
     * Sets the remotejobLocationRequirements
     * 
     * @param string $remotejobLocationRequirements
     * @return void
     */
    public function setRemotejobLocationRequirements($remotejobLocationRequirements)
    {
        $this->remotejobLocationRequirements = $remotejobLocationRequirements;
    }

    /**
     * Returns the datePosted
     * 
     * @return \DateTime $datePosted
     */
    public function getDatePosted()
    {
        return $this->datePosted;
    }

    /**
     * Sets the datePosted
     * 
     * @param \DateTime $datePosted
     * @return void
     */
    public function setDatePosted(\DateTime $datePosted)
    {
        $this->datePosted = $datePosted;
    }

    /**
     * Returns the validThrough
     * 
     * @return \DateTime $validThrough
     */
    public function getValidThrough()
    {
        return $this->validThrough;
    }

    /**
     * Sets the validThrough
     * 
     * @param \DateTime $validThrough
     * @return void
     */
    public function setValidThrough(\DateTime $validThrough)
    {
        $this->validThrough = $validThrough;
    }
}
