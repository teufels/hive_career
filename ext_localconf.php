<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCareer',
            'Hivecareerlist',
            [
                'JobOffer' => 'list'
            ],
            // non-cacheable actions
            [
                'JobOffer' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCareer',
            'Hivecareershow',
            [
                'JobOffer' => 'list, show'
            ],
            // non-cacheable actions
            [
                'JobOffer' => ''
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        hivecareerlist {
                            iconIdentifier = hive_career-plugin-hivecareerlist
                            title = LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hive_career_hivecareerlist.name
                            description = LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hive_career_hivecareerlist.description
                            tt_content_defValues {
                                CType = list
                                list_type = hivecareer_hivecareerlist
                            }
                        }
                        hivecareershow {
                            iconIdentifier = hive_career-plugin-hivecareershow
                            title = LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hive_career_hivecareershow.name
                            description = LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hive_career_hivecareershow.description
                            tt_content_defValues {
                                CType = list
                                list_type = hivecareer_hivecareershow
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'hive_career-plugin-hivecareerlist',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:hive_career/Resources/Public/Icons/user_plugin_hivecareerlist.svg']
			);

			$iconRegistry->registerIcon(
				'hive_career-plugin-hivecareershow',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:hive_career/Resources/Public/Icons/user_plugin_hivecareershow.svg']
			);

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function()
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecareerlist >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecareerlist {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Jobs (list)
                            description = List of job offers
                            tt_content_defValues {
                                CType = list
                                list_type = hivecareer_hivecareerlist
                            }
                        }
                        show := addToList(hivecareerlist)
                    }

                }
            }'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecareershow >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecareershow {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Jobs (Detail)
                            description = Jobs in detail
                            tt_content_defValues {
                                CType = list
                                list_type = hivecareer_hivecareershow
                            }
                        }
                        show := addToList(hivecareershow)
                    }

                }
            }'
        );

    }
);
