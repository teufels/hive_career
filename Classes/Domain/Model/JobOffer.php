<?php
namespace HIVE\HiveCareer\Domain\Model;


/***
 *
 * This file is part of the "hive_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/
/**
 * JobOffer
 */
class JobOffer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * backendTitle
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $backendTitle = '';

    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate = null;

    /**
     * employmenttype
     *
     * @var string
     */
    protected $employmenttype = '';

    /**
     * company
     *
     * @var string
     */
    protected $company = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * intro
     *
     * @var string
     */
    protected $intro = '';

    /**
     * task
     *
     * @var string
     */
    protected $task = '';

    /**
     * profile
     *
     * @var string
     */
    protected $profile = '';

    /**
     * information
     *
     * @var string
     */
    protected $information = '';

    /**
     * download
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $download = null;

    /**
     * externalLink
     *
     * @var string
     */
    protected $externalLink = '';

    /**
     * googleforjobs
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCareer\Domain\Model\GoogleForJobs>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $googleforjobs = null;

    /**
     * contact
     *
     * @var \HIVE\HiveCptCntContactperson\Domain\Model\Contactperson
     */
    protected $contact = null;

    /**
     * pathSegment
     *
     * @var string
     */
    protected $pathSegment;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->googleforjobs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the startDate
     *
     * @param \DateTime $startDate
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the employmenttype
     *
     * @return string $employmenttype
     */
    public function getEmploymenttype()
    {
        return $this->employmenttype;
    }

    /**
     * Sets the employmenttype
     *
     * @param string $employmenttype
     * @return void
     */
    public function setEmploymenttype($employmenttype)
    {
        $this->employmenttype = $employmenttype;
    }

    /**
     * Returns the company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Returns the location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param string $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Returns the intro
     *
     * @return string $intro
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Sets the intro
     *
     * @param string $intro
     * @return void
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * Returns the task
     *
     * @return string $task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Sets the task
     *
     * @param string $task
     * @return void
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * Returns the profile
     *
     * @return string $profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Sets the profile
     *
     * @param string $profile
     * @return void
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * Returns the information
     *
     * @return string $information
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Sets the information
     *
     * @param string $information
     * @return void
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * Returns the download
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $download
     */
    public function getDownload()
    {
        return $this->download;
    }

    /**
     * Sets the download
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $download
     * @return void
     */
    public function setDownload(\TYPO3\CMS\Extbase\Domain\Model\FileReference $download)
    {
        $this->download = $download;
    }

    /**
     * Adds a GoogleForJobs
     *
     * @param \HIVE\HiveCareer\Domain\Model\GoogleForJobs $googleforjob
     * @return void
     */
    public function addGoogleforjob(\HIVE\HiveCareer\Domain\Model\GoogleForJobs $googleforjob)
    {
        $this->googleforjobs->attach($googleforjob);
    }

    /**
     * Removes a GoogleForJobs
     *
     * @param \HIVE\HiveCareer\Domain\Model\GoogleForJobs $googleforjobToRemove The GoogleForJobs to be removed
     * @return void
     */
    public function removeGoogleforjob(\HIVE\HiveCareer\Domain\Model\GoogleForJobs $googleforjobToRemove)
    {
        $this->googleforjobs->detach($googleforjobToRemove);
    }

    /**
     * Returns the googleforjobs
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCareer\Domain\Model\GoogleForJobs> $googleforjobs
     */
    public function getGoogleforjobs()
    {
        return $this->googleforjobs;
    }

    /**
     * Sets the googleforjobs
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCareer\Domain\Model\GoogleForJobs> $googleforjobs
     * @return void
     */
    public function setGoogleforjobs(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $googleforjobs)
    {
        $this->googleforjobs = $googleforjobs;
    }

    /**
     * Returns the externalLink
     *
     * @return string $externalLink
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * Sets the externalLink
     *
     * @param string $externalLink
     * @return void
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;
    }

    /**
     * Returns the contact
     *
     * @return \HIVE\HiveCptCntContactperson\Domain\Model\Contactperson $contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Sets the contact
     *
     * @param \HIVE\HiveCptCntContactperson\Domain\Model\Contactperson $contact
     * @return void
     */
    public function setContact(\HIVE\HiveCptCntContactperson\Domain\Model\Contactperson $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get path segment
     *
     * @return string
     */
    public function getPathSegment()
    {
        return $this->pathSegment;
    }

    /**
     * Set path segment
     *
     * @param string $pathSegment
     */
    public function setPathSegment($pathSegment)
    {
        $this->pathSegment = $pathSegment;
    }
}
