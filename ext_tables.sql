#
# Table structure for table 'tx_hivecareer_domain_model_joboffer'
#
CREATE TABLE tx_hivecareer_domain_model_joboffer (

	backend_title varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	start_date date DEFAULT NULL,
	employmenttype varchar(255) DEFAULT '' NOT NULL,
	company varchar(255) DEFAULT '' NOT NULL,
	location varchar(255) DEFAULT '' NOT NULL,
	intro text,
	task text,
	profile text,
	information text,
	download int(11) unsigned NOT NULL default '0',
	external_link varchar(255) DEFAULT '' NOT NULL,
	googleforjobs int(11) unsigned DEFAULT '0' NOT NULL,
	contact int(11) unsigned DEFAULT '0',
	path_segment varchar(2048),

);

#
# Table structure for table 'tx_hivecareer_domain_model_googleforjobs'
#
CREATE TABLE tx_hivecareer_domain_model_googleforjobs (

	joboffer int(11) unsigned DEFAULT '0' NOT NULL,

	backend_title varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	employmenttype varchar(255) DEFAULT '' NOT NULL,
	identifier_name varchar(255) DEFAULT '' NOT NULL,
	identifier_value varchar(255) DEFAULT '' NOT NULL,
	hiringorganization_name varchar(255) DEFAULT '' NOT NULL,
	hiringorganization_website varchar(255) DEFAULT '' NOT NULL,
	hiringorganization_logo int(11) unsigned NOT NULL default '0',
	joblocation_address_street varchar(255) DEFAULT '' NOT NULL,
	joblocation_address_postalcode varchar(255) DEFAULT '' NOT NULL,
	joblocation_address_city varchar(255) DEFAULT '' NOT NULL,
	joblocation_address_region varchar(255) DEFAULT '' NOT NULL,
	joblocation_address_country varchar(255) DEFAULT '' NOT NULL,
	basesalary_currency varchar(255) DEFAULT '' NOT NULL,
	basesalary_unit varchar(255) DEFAULT '' NOT NULL,
	basesalary_value double(11,2) DEFAULT '0.00' NOT NULL,
	remotejob smallint(5) unsigned DEFAULT '0' NOT NULL,
	remotejob_location_requirements varchar(255) DEFAULT '' NOT NULL,
	date_posted date DEFAULT NULL,
	valid_through date DEFAULT NULL

);

#
# Table structure for table 'tx_hivecareer_domain_model_joboffer'
#
CREATE TABLE tx_hivecareer_domain_model_joboffer (
	categories int(11) unsigned DEFAULT '0' NOT NULL
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
