<?php
namespace HIVE\HiveCareer\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class GoogleForJobsTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \HIVE\HiveCareer\Domain\Model\GoogleForJobs
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCareer\Domain\Model\GoogleForJobs();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );
    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmploymenttypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmploymenttype()
        );
    }

    /**
     * @test
     */
    public function setEmploymenttypeForStringSetsEmploymenttype()
    {
        $this->subject->setEmploymenttype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'employmenttype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIdentifierNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIdentifierName()
        );
    }

    /**
     * @test
     */
    public function setIdentifierNameForStringSetsIdentifierName()
    {
        $this->subject->setIdentifierName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'identifierName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIdentifierValueReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIdentifierValue()
        );
    }

    /**
     * @test
     */
    public function setIdentifierValueForStringSetsIdentifierValue()
    {
        $this->subject->setIdentifierValue('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'identifierValue',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHiringorganizationNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHiringorganizationName()
        );
    }

    /**
     * @test
     */
    public function setHiringorganizationNameForStringSetsHiringorganizationName()
    {
        $this->subject->setHiringorganizationName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'hiringorganizationName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHiringorganizationWebsiteReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHiringorganizationWebsite()
        );
    }

    /**
     * @test
     */
    public function setHiringorganizationWebsiteForStringSetsHiringorganizationWebsite()
    {
        $this->subject->setHiringorganizationWebsite('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'hiringorganizationWebsite',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHiringorganizationLogoReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getHiringorganizationLogo()
        );
    }

    /**
     * @test
     */
    public function setHiringorganizationLogoForFileReferenceSetsHiringorganizationLogo()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setHiringorganizationLogo($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'hiringorganizationLogo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJoblocationAddressStreetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJoblocationAddressStreet()
        );
    }

    /**
     * @test
     */
    public function setJoblocationAddressStreetForStringSetsJoblocationAddressStreet()
    {
        $this->subject->setJoblocationAddressStreet('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'joblocationAddressStreet',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJoblocationAddressPostalcodeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJoblocationAddressPostalcode()
        );
    }

    /**
     * @test
     */
    public function setJoblocationAddressPostalcodeForStringSetsJoblocationAddressPostalcode()
    {
        $this->subject->setJoblocationAddressPostalcode('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'joblocationAddressPostalcode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJoblocationAddressCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJoblocationAddressCity()
        );
    }

    /**
     * @test
     */
    public function setJoblocationAddressCityForStringSetsJoblocationAddressCity()
    {
        $this->subject->setJoblocationAddressCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'joblocationAddressCity',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJoblocationAddressRegionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJoblocationAddressRegion()
        );
    }

    /**
     * @test
     */
    public function setJoblocationAddressRegionForStringSetsJoblocationAddressRegion()
    {
        $this->subject->setJoblocationAddressRegion('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'joblocationAddressRegion',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJoblocationAddressCountryReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJoblocationAddressCountry()
        );
    }

    /**
     * @test
     */
    public function setJoblocationAddressCountryForStringSetsJoblocationAddressCountry()
    {
        $this->subject->setJoblocationAddressCountry('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'joblocationAddressCountry',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBasesalaryCurrencyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBasesalaryCurrency()
        );
    }

    /**
     * @test
     */
    public function setBasesalaryCurrencyForStringSetsBasesalaryCurrency()
    {
        $this->subject->setBasesalaryCurrency('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'basesalaryCurrency',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBasesalaryUnitReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBasesalaryUnit()
        );
    }

    /**
     * @test
     */
    public function setBasesalaryUnitForStringSetsBasesalaryUnit()
    {
        $this->subject->setBasesalaryUnit('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'basesalaryUnit',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBasesalaryValueReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getBasesalaryValue()
        );
    }

    /**
     * @test
     */
    public function setBasesalaryValueForFloatSetsBasesalaryValue()
    {
        $this->subject->setBasesalaryValue(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'basesalaryValue',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getRemotejobReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getRemotejob()
        );
    }

    /**
     * @test
     */
    public function setRemotejobForBoolSetsRemotejob()
    {
        $this->subject->setRemotejob(true);

        self::assertAttributeEquals(
            true,
            'remotejob',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRemotejobLocationRequirementsReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRemotejobLocationRequirements()
        );
    }

    /**
     * @test
     */
    public function setRemotejobLocationRequirementsForStringSetsRemotejobLocationRequirements()
    {
        $this->subject->setRemotejobLocationRequirements('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'remotejobLocationRequirements',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDatePostedReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDatePosted()
        );
    }

    /**
     * @test
     */
    public function setDatePostedForDateTimeSetsDatePosted()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDatePosted($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'datePosted',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getValidThroughReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getValidThrough()
        );
    }

    /**
     * @test
     */
    public function setValidThroughForDateTimeSetsValidThrough()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setValidThrough($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'validThrough',
            $this->subject
        );
    }
}
