<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');


$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = '
                                        --div--;General, sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title,
                                        --div--;Job, title, description, employmenttype, date_posted, valid_through,
                                        --div--;Job Location, joblocation_address_street, joblocation_address_postalcode, joblocation_address_city, joblocation_address_region, joblocation_address_country,
                                        --div--;Remote Job, remotejob, remotejob_location_requirements,
                                        --div--;Base Salery, basesalary_unit, basesalary_value, basesalary_currency,
                                        --div--;Organization, hiringorganization_name, hiringorganization_website, hiringorganization_logo,
                                        ,--palette--;Identifier, identifier_name, identifier_value,
                                        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime';


$GLOBALS['TCA'][$sModel]['columns']['backend_title'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.backend_title',
    'config' => [
        'type' => 'input',
        'size' => 30,
        #'eval' => 'null,trim,required',
        #'placeholder' => 'G4JOBS',
        #'mode' => 'useOrOverridePlaceholder',
        'eval' => 'trim,required',
        'default' => 'G4JOBS',
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['employmenttype'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.employmenttype',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['Full-time', 'FULL_TIME'],
            ['Part-time', 'PART_TIME'],
            ['Contractor', 'CONTRACTOR'],
            ['Temporary', 'TEMPORARY'],
            ['Intern', 'INTERN'],
            ['Volunteer', 'VOLUNTEER'],
            ['Per diem', 'PER_DIEM'],
            ['Other', 'OTHER'],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => 'required'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['identifier_name'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.identifier_name',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['identifier_value'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.identifier_value',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['hiringorganization_name'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_website',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['hiringorganization_website'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_website',
    'config' => [
        'type' => 'input',
        'renderType' => 'inputLink',
        'size' => 30,
        'eval' => 'trim'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['hiringorganization_logo'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.hiringorganization_logo',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'hiringorganization_logo',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
            ],
            'foreign_types' => [
                '0' => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                ]
            ],
            'maxitems' => 1
        ],
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
    ),
];

$GLOBALS['TCA'][$sModel]['columns']['joblocation_address_street'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_street',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['joblocation_address_postalcode'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_postalcode',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['joblocation_address_city'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_city',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
 $GLOBALS['TCA'][$sModel]['columns']['joblocation_address_region'] = [
    'exclude' => true,
     'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_region',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['joblocation_address_country'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.joblocation_address_country',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];


$GLOBALS['TCA'][$sModel]['columns']['basesalary_currency'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_currency',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['basesalary_unit'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_unit',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['Hour', 'HOUR'],
            ['Day', 'DAY'],
            ['Week', 'WEEK'],
            ['Month', 'MONTH'],
            ['Intern', 'INTERN'],
            ['Year', 'YEAR'],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => ''
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['basesalary_value'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.basesalary_value',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'double2'
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['remotejob'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.remotejob',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
        'default' => 0,
    ]
];

$GLOBALS['TCA'][$sModel]['columns']['remotejob_location_requirements'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:hive_career/Resources/Private/Language/locallang_db.xlf:tx_hivecareer_domain_model_googleforjobs.remotejob_location_requirements',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
